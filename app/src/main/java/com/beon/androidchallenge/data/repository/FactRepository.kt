package com.beon.androidchallenge.data.repository

import com.beon.androidchallenge.domain.model.Fact

class FactRepository(
    private var factRemoteDataSource: FactRemoteDataSource,
    private var factLocalDataSource: FactLocalDataSource
) {

    companion object {
        private var INSTANCE: FactRepository? = null

        fun getInstance(): FactRepository {
            if (INSTANCE == null) {
                INSTANCE = FactRepository(FactRemoteDataSource(), FactLocalDataSource())
            }
            return INSTANCE!!
        }
    }

    fun getFactForNumber(number: String, callback: FactRepositoryCallback<Fact>) {
        factRemoteDataSource.getFactForNumber(number,
            object : FactRemoteDataSource.GetFactForNumberCallback {
                override fun isLoading() {
                    callback.isLoading()
                }
                override fun onFactLoaded(fact: Fact) {
                    callback.onResponse(fact)
                }

                override fun onFactLoadFailed(message: String) {
                    callback.onError(message)
                }
            })
    }

    interface FactRepositoryCallback<T> {
        fun isLoading()
        fun onResponse(response: T)
        fun onError(message: String)
    }

}