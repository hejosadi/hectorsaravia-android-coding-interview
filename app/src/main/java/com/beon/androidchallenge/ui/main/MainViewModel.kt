package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

const val DELAY = 1000L

class MainViewModel : ViewModel() {

    val currentFact = MutableLiveData<Fact?>(null)

    val isLoading = MutableLiveData(false)
    val error = MutableLiveData<String?>(null)
    private var job: Job? = null
    fun searchNumberFact(number: String) {
        job?.cancel()
        job = viewModelScope.launch {
            delay(DELAY)
            if (number.isEmpty()) {
                currentFact.postValue(null)
                return@launch
            }
            fetchFactForNumber(number)

        }
    }

    private fun fetchFactForNumber(number: String) {
        FactRepository.getInstance()
            .getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
                override fun isLoading() {
                    isLoading.postValue(true)
                }

                override fun onResponse(response: Fact) {
                    clear()
                    currentFact.postValue(response)
                }

                override fun onError(message: String) {
                    clear()
                    error.postValue(message)
                }
            })
    }

    private fun clear() {
        error.postValue(null)
        isLoading.postValue(false)
    }
}